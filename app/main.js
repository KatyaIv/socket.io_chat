require(['class/chat','class/auth'], function (_chatModule, _authModule) {

    var mainAuth = new _authModule();

    mainAuth.onSuccess(function(){
        var ChatLocal = new _chatModule({
            url: 'localhost:3000',
        });

        ChatLocal.init();
        console.log(ChatLocal.config.url);
    });

    mainAuth.onError(function(){
        alert( "Логин или пароль введены неверно!" );
    });

    mainAuth.init();
});