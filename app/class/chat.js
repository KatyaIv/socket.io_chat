/**
 * Created by Катя on 02-Mar-16.
 */
define(
    ['../template/chat/main_template'],
    function (_chatForm) {
        var _chatModule = function (_config) {

            var chat = {
                selector: false,
                formMsgText: false,
                formJoinUser: false,
                username: false,
                config: {
                    url: _config.url || 'localhost:3000',
                }
            };

            chat.init = function () {
                chat.selector = _chatForm();
                $('body').prepend(chat.selector);
                chat.formMsgText = $(chat.selector.find('.msgText'));
                chat.formJoinUser = $(chat.selector.find('.joinUser'));

                if (chat.selector.length == 0) return false;

                chat.attachEvent_newUser();
                chat.attachEvent_sendMsg();

                return true;
            };

            chat.attachEvent_newUser = function () {
                chat.formJoinUser.submit(function () {
                    var _inpUsername = $(chat.formJoinUser.find(':input[type="text"]'));
                    chat.username = _inpUsername.val();

                    socket.emit('user joined', chat.username);

                    chat.formJoinUser.detach();
                    chat.formMsgText.show();

                    return false;
                });
            };

            chat.attachEvent_sendMsg = function () {
                chat.formMsgText.submit(function () {
                    var _inpMsgText = $(chat.formMsgText.find(':input[type="text"]'));

                    socket.emit('chat message', '<' + chat.username + '> ' + _inpMsgText.val());

                    _inpMsgText.val('');

                    return false;
                });
            };

            var socket = io(chat.config.url);

            socket.on('user joined', function (name) {
                chat.selector.find('.messages').append($('<h3>').text('Пользователь <' + name + '> вошел в чат'));
            });

            socket.on('chat message', function (msg) {
                chat.selector.find('.messages').append($('<li>').text(msg));
            });

            socket.on('user disconnected', function (name) {
                chat.selector.find('.messages').append($('<h3>').text('Пользователь <' + name + '> вышел из чата'));
            });

            window.onbeforeunload = function () {
                socket.emit('user disconnected', chat.username);
            };

            return chat;
        };
        return _chatModule;
    });

