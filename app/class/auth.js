/**
 * Created by Катя on 10-Mar-16.
 */

define(
    ['../template/auth/main_auth'],
    function (_authTemplate) {
        var _authModule = function () {

            var auth = {
                event: {
                    selector: false,
                    inputLogin: false,
                    inputPassword: false,
                    success: false,
                    error: false
                }
            };

            auth.init = function () {
                auth.selector = _authTemplate();

                if (auth.selector.length == 0) return false;

                $('body').prepend(auth.selector);
                auth.inputLogin = auth.selector.find(':input[placeholder="Логин"]');
                auth.inputPassword = auth.selector.find(':input[type="password"]');
                auth.attachEvent_Auth();

                return true;
            };

            auth.onSuccess = function (callback) {
                auth.event.success = callback;
            };

            auth.onError = function (callback) {
                auth.event.error = callback;
            };

            auth.attachEvent_Auth = function () {
                auth.selector.submit(function () {
                    var _login = auth.inputLogin.val();
                    var _password = auth.inputPassword.val();

                    if (_login == 'test' && _password == '1234') {
                        auth.selector.detach();
                        auth.event.success();
                    } else {
                        auth.event.error();
                        auth.inputPassword.val('');
                    }

                    return false;
                });
            };

        return auth;
        };
    return _authModule;
    });