/**
 * Created by Катя on 05-Mar-16.
 */

define(function () {
    var _chatForm = function () {
        var _mainCont = $('<div/>');
        var _ul = $('<ul class="messages"/>');
        var _formMsgText = $('<form class="msgText" hidden="true" />');
        var _inputMsgText = $('<input type="text" autocomplete="off" />');
        var _btnMsgText = $('<button>Отправить</button>');
        var _formJoinUser = $('<form class="joinUser">');
        var _inputJoinUser = $('<input type="text" placeholder="Введите имя пользователя" />');
        var _btnJoinUser = $('<button>Присоединиться</button>');

        _formMsgText.append([
            _inputMsgText,
            _btnMsgText
        ]);
        _formJoinUser.append([
            _inputJoinUser,
            _btnJoinUser
        ]);
        _mainCont.append([
            _ul,
            _formMsgText,
            _formJoinUser
        ]);

        return _mainCont;
    };
    return _chatForm;
});