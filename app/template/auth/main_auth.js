/**
 * Created by Катя on 05-Mar-16.
 */

define(function () {
    var _authTemplate = function () {
        var _mainCont = $('<div/>');
        var _formAuth = $('<form class="auth"/>');
        var _inputLogin = $('<input type="text" placeholder="Логин"/>');
        var _inputPassword = $('<input type="password" placeholder="Пароль"/>');
        var _btnEnter = $('<button>Вход</button>');

        _formAuth.append([
            _inputLogin,
            _inputPassword,
            _btnEnter
        ]);

        _mainCont.append([
            _formAuth
        ]);

        return _mainCont;
    };
    return _authTemplate;
});