var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;

app.use(express.static(__dirname));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
    console.log('new user connected');

    socket.on('user joined', function (name) {
        socket.broadcast.emit('user joined', name);
    });

    socket.on('chat message', function (msg) {
        io.emit('chat message', msg);
    });

    socket.on('user disconnected', function (name) {
        socket.broadcast.emit('user disconnected', name);
    });

    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
});

http.listen(port, function () {
    console.log('Server listening at port %d', port);
});
